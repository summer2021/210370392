# Typed Parser Generator

Generating efficient and expressive parsers, for multiple programming languages, from one grammar.

Goals:
- Analysis
    - [X] Parsing frontend BNF
    - [X] IR for parsing(**FIR**)
    - [X] Resolving left recursions
    - [ ] Type inference
- [ ] CodeGen
- Compiler Distribution
    - [X] JavaScript distribution
    - [ ] Julia distribution

P.S: For our goal in Summer 2021, type inference for the grammar is not required. It is a future plan to support CodeGen targets like C++/Java.

## Build(JS distribution)

```
$> bash ./build.sh
```

## Test

```
$> bash ./test.sh
```
