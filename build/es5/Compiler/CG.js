import { comparePrimitives, getEnumerator } from "../.fable/fable-library.3.2.9/Util.js";
import { $007CKV$007C } from "./IR.js";
import { addToSet, getItemFromDict } from "../.fable/fable-library.3.2.9/MapUtil.js";
import { ofSeq, FSharpMap__get_Item } from "../.fable/fable-library.3.2.9/Map.js";
import { tail, head, isEmpty } from "../.fable/fable-library.3.2.9/List.js";
import { singleton, collect, delay, iterate } from "../.fable/fable-library.3.2.9/Seq.js";
import { ofSeq as ofSeq_1 } from "../.fable/fable-library.3.2.9/Set.js";

export const bof_token_id = -1;

export const eof_token_id = -2;

export function firstSets(g) {
    const m = new Map([]);
    const enumerator = getEnumerator(g);
    try {
        while (enumerator["System.Collections.IEnumerator.MoveNext"]()) {
            m.set($007CKV$007C(enumerator["System.Collections.Generic.IEnumerator`1.get_Current"]())[0], new Set([]));
        }
    }
    finally {
        enumerator.Dispose();
    }
    const applyEach = (n_1) => {
        if (m.has(n_1)) {
            return getItemFromDict(m, n_1);
        }
        else {
            const first_set = new Set([]);
            m.set(n_1, first_set);
            const enumerator_1 = getEnumerator(FSharpMap__get_Item(g, n_1).NotLR);
            try {
                while (enumerator_1["System.Collections.IEnumerator.MoveNext"]()) {
                    const value = applyEachWithProds(n_1)(enumerator_1["System.Collections.Generic.IEnumerator`1.get_Current"]());
                }
            }
            finally {
                enumerator_1.Dispose();
            }
            return first_set;
        }
    };
    const applyEachWithProds = (n_2) => ((notLR_prod) => {
        const first_set_1 = getItemFromDict(m, n_2);
        if (!isEmpty(notLR_prod)) {
            if (head(notLR_prod).tag === 4) {
                void addToSet(head(notLR_prod).fields[0], first_set_1);
            }
            else if (head(notLR_prod).tag === 5) {
                iterate((arg) => {
                    void addToSet(arg, first_set_1);
                }, applyEach(head(notLR_prod).fields[0]));
            }
            else {
                applyEachWithProds(n_2)(tail(notLR_prod));
            }
        }
    });
    const enumerator_2 = getEnumerator(g);
    try {
        while (enumerator_2["System.Collections.IEnumerator.MoveNext"]()) {
            const activePatternResult264 = $007CKV$007C(enumerator_2["System.Collections.Generic.IEnumerator`1.get_Current"]());
            const enumerator_3 = getEnumerator(activePatternResult264[1].NotLR);
            try {
                while (enumerator_3["System.Collections.IEnumerator.MoveNext"]()) {
                    const value_3 = applyEachWithProds(activePatternResult264[0])(enumerator_3["System.Collections.Generic.IEnumerator`1.get_Current"]());
                }
            }
            finally {
                enumerator_3.Dispose();
            }
        }
    }
    finally {
        enumerator_2.Dispose();
    }
    return ofSeq(delay(() => collect((matchValue) => {
        const activePatternResult269 = $007CKV$007C(matchValue);
        return singleton([activePatternResult269[0], ofSeq_1(activePatternResult269[1], {
            Compare: (x, y) => comparePrimitives(x, y),
        })]);
    }, m)));
}

export function codegen(token_values, g) {
    throw (new Error(""));
}

