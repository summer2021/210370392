import { Record, Union } from "../.fable/fable-library.3.2.9/Types.js";
import { record_type, option_type, union_type, tuple_type, list_type, float64_type, bool_type, string_type, int32_type } from "../.fable/fable-library.3.2.9/Reflection.js";
import { ofSeq as ofSeq_1, length, map as map_1, concat, append, singleton, empty, tail, head, cons, isEmpty } from "../.fable/fable-library.3.2.9/List.js";
import { singleton as singleton_1, fold, map, collect, delay, toList } from "../.fable/fable-library.3.2.9/Seq.js";
import { ofSeq, empty as empty_1, add, tryFind } from "../.fable/fable-library.3.2.9/Map.js";

export function $007CKV$007C(kv) {
    return [kv[0], kv[1]];
}

export class CLang extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["CSlot", "CVar", "CNone", "CInt", "CFloat", "CStr", "CBool", "CTuple", "CCall", "CAttr", "CBlock", "CAnd", "COr", "CProj", "CDefun", "CAssign", "CSwitch", "CWhile", "CIf"];
    }
}

export function CLang$reflection() {
    return union_type("TypedPG.Compiler.IR.CLang", [], CLang, () => [[["Item", int32_type]], [["name", string_type], ["is_builtin", bool_type]], [], [["Item", int32_type]], [["Item", float64_type]], [["Item", string_type]], [["Item", bool_type]], [["Item", list_type(CLang$reflection())]], [["Item1", CLang$reflection()], ["Item2", list_type(CLang$reflection())]], [["Item1", CLang$reflection()], ["Item2", string_type]], [["Item", list_type(CLang$reflection())]], [["Item1", CLang$reflection()], ["Item2", CLang$reflection()]], [["Item1", CLang$reflection()], ["Item2", CLang$reflection()]], [["Item1", CLang$reflection()], ["Item2", int32_type]], [["Item1", string_type], ["parameters", list_type(string_type)], ["body", CLang$reflection()]], [["name", string_type], ["Item2", CLang$reflection()]], [["Item1", CLang$reflection()], ["Item2", list_type(tuple_type(int32_type, CLang$reflection()))], ["Item3", CLang$reflection()]], [["Item1", CLang$reflection()], ["Item2", CLang$reflection()]], [["Item1", CLang$reflection()], ["Item2", CLang$reflection()], ["Item3", CLang$reflection()]]]);
}

export class PE extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["PAnd", "POr", "POpt", "PBind", "PTerm", "PNonTerm"];
    }
}

export function PE$reflection() {
    return union_type("TypedPG.Compiler.IR.PE", [], PE, () => [[["Item", list_type(PE$reflection())]], [["Item", list_type(PE$reflection())]], [["Item", PE$reflection()]], [["name", string_type], ["Item2", PE$reflection()]], [["Item", string_type]], [["Item", string_type]]]);
}

export class ProdRHS extends Record {
    constructor(parser, action) {
        super();
        this.parser = parser;
        this.action = action;
    }
}

export function ProdRHS$reflection() {
    return record_type("TypedPG.Compiler.IR.ProdRHS", [], ProdRHS, () => [["parser", PE$reflection()], ["action", option_type(CLang$reflection())]]);
}

export class FIR extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["FStore", "FPack", "FMkCST", "FExpr", "FTerm", "FNonTerm", "FScope"];
    }
}

export function FIR$reflection() {
    return union_type("TypedPG.Compiler.IR.FIR", [], FIR, () => [[["name", string_type]], [["count", int32_type]], [["name", string_type]], [["code", CLang$reflection()]], [["Item", string_type]], [["Item", string_type]], [["nonterminal_name", string_type], ["Item2", ScopeAction$reflection()]]]);
}

export class ScopeAction extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["Push", "Pop"];
    }
}

export function ScopeAction$reflection() {
    return union_type("TypedPG.Compiler.IR.ScopeAction", [], ScopeAction, () => [[], []]);
}

export function cartesian_product(_arg1) {
    if (!isEmpty(_arg1)) {
        return toList(delay(() => collect((tl) => map((hd) => cons(hd, tl), head(_arg1)), cartesian_product(tail(_arg1)))));
    }
    else {
        return singleton(empty());
    }
}

export function push_back(a, xs) {
    return append(xs, singleton(a));
}

export function flatten_parsing_expression(_arg1) {
    switch (_arg1.tag) {
        case 1: {
            return concat(map_1((_arg1_3) => flatten_parsing_expression(_arg1_3), _arg1.fields[0]));
        }
        case 4: {
            return singleton(singleton(new FIR(4, _arg1.fields[0])));
        }
        case 5: {
            return singleton(singleton(new FIR(5, _arg1.fields[0])));
        }
        case 3: {
            return map_1((xs_1) => push_back(new FIR(0, _arg1.fields[0]), xs_1), flatten_parsing_expression(_arg1.fields[1]));
        }
        case 2: {
            return append(singleton(singleton(new FIR(1, 0))), flatten_parsing_expression(_arg1.fields[0]));
        }
        default: {
            const elements = _arg1.fields[0];
            const len = length(elements) | 0;
            return map_1((arg) => push_back(new FIR(1, len), concat(arg)), cartesian_product(map_1((_arg1_1) => flatten_parsing_expression(_arg1_1), elements)));
        }
    }
}

export function Seq_groupBy(func, seq) {
    return fold((state, e) => {
        const key = func(e);
        const matchValue = tryFind(key, state);
        if (matchValue != null) {
            const xs_1 = matchValue;
            const value_1 = void (xs_1.push(e));
            return state;
        }
        else {
            const xs = [];
            const value = void (xs.push(e));
            return add(key, xs, state);
        }
    }, empty_1(), seq);
}

export function pgrammar_to_fgrammar(pgrammar) {
    return ofSeq(delay(() => collect((matchValue) => {
        const activePatternResult157 = $007CKV$007C(matchValue);
        return singleton_1([activePatternResult157[0], ofSeq_1(delay(() => collect((matchValue_1) => {
            let expr;
            const action = matchValue_1.action;
            return ((action != null) ? ((expr = action, (list) => map_1((xs) => push_back(new FIR(3, expr), xs), list))) : ((x) => x))(flatten_parsing_expression(matchValue_1.parser));
        }, activePatternResult157[1])))]);
    }, pgrammar)));
}

