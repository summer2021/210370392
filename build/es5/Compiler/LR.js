import { FSharpException, Union, Record } from "../.fable/fable-library.3.2.9/Types.js";
import { $007CKV$007C, FIR, ScopeAction, FIR$reflection } from "./IR.js";
import { class_type, union_type, record_type, list_type } from "../.fable/fable-library.3.2.9/Reflection.js";
import { comparePrimitives, getEnumerator, equals } from "../.fable/fable-library.3.2.9/Util.js";
import { FSharpMap__get_Item, ofSeq, tryFind, ofList } from "../.fable/fable-library.3.2.9/Map.js";
import { empty as empty_2, isEmpty as isEmpty_1, singleton, collect, map, delay, toList } from "../.fable/fable-library.3.2.9/Seq.js";
import { addToSet } from "../.fable/fable-library.3.2.9/MapUtil.js";
import { ofSeq as ofSeq_1, cons, tail, singleton as singleton_1, append, head, empty, isEmpty } from "../.fable/fable-library.3.2.9/List.js";
import { empty as empty_1, contains, add } from "../.fable/fable-library.3.2.9/Set.js";

export class FGrammarExRHS extends Record {
    constructor(LR, NotLR, IsEmpty) {
        super();
        this.LR = LR;
        this.NotLR = NotLR;
        this.IsEmpty = IsEmpty;
    }
}

export function FGrammarExRHS$reflection() {
    return record_type("TypedPG.Compiler.LR.FGrammarExRHS", [], FGrammarExRHS, () => [["LR", list_type(list_type(FIR$reflection()))], ["NotLR", list_type(list_type(FIR$reflection()))], ["IsEmpty", list_type(list_type(FIR$reflection()))]]);
}

export class LRKind extends Union {
    constructor(tag, ...fields) {
        super();
        this.tag = (tag | 0);
        this.fields = fields;
    }
    cases() {
        return ["IsLR", "NotLR", "IsEmpty"];
    }
}

export function LRKind$reflection() {
    return union_type("TypedPG.Compiler.LR.LRKind", [], LRKind, () => [[], [], []]);
}

export class KeyNotFound extends FSharpException {
    constructor(Data0) {
        super();
        this.Data0 = Data0;
    }
}

export function KeyNotFound$reflection() {
    return class_type("TypedPG.Compiler.LR.KeyNotFound", void 0, KeyNotFound, class_type("System.Exception"));
}

function KeyNotFound__Equals_229D3F39(this$, obj) {
    if (!equals(this$, null)) {
        if (!equals(obj, null)) {
            if (obj instanceof KeyNotFound) {
                return equals(this$.Data0, obj.Data0);
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else if (!equals(obj, null)) {
        return false;
    }
    else {
        return true;
    }
}

export class UnknownNonTerminalName extends FSharpException {
    constructor(Data0) {
        super();
        this.Data0 = Data0;
    }
}

export function UnknownNonTerminalName$reflection() {
    return class_type("TypedPG.Compiler.LR.UnknownNonTerminalName", void 0, UnknownNonTerminalName, class_type("System.Exception"));
}

function UnknownNonTerminalName__Equals_229D3F39(this$, obj) {
    if (!equals(this$, null)) {
        if (!equals(obj, null)) {
            if (obj instanceof UnknownNonTerminalName) {
                return this$.Data0 === obj.Data0;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else if (!equals(obj, null)) {
        return false;
    }
    else {
        return true;
    }
}

export class NullableLeftRecursion extends FSharpException {
    constructor(nonterminal_name) {
        super();
        this.nonterminal_name = nonterminal_name;
    }
}

export function NullableLeftRecursion$reflection() {
    return class_type("TypedPG.Compiler.LR.NullableLeftRecursion", void 0, NullableLeftRecursion, class_type("System.Exception"));
}

function NullableLeftRecursion__Equals_229D3F39(this$, obj) {
    if (!equals(this$, null)) {
        if (!equals(obj, null)) {
            if (obj instanceof NullableLeftRecursion) {
                return this$.nonterminal_name === obj.nonterminal_name;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    else if (!equals(obj, null)) {
        return false;
    }
    else {
        return true;
    }
}

export function groupByUnder(seq) {
    const init = ofList(toList(delay(() => map((each) => [each, []], [new LRKind(0), new LRKind(1), new LRKind(2)]))));
    const enumerator = getEnumerator(seq);
    try {
        while (enumerator["System.Collections.IEnumerator.MoveNext"]()) {
            const e = enumerator["System.Collections.Generic.IEnumerator`1.get_Current"]();
            const key = e.kind;
            const matchValue = tryFind(key, init);
            if (matchValue != null) {
                const xs = matchValue;
                const value = void (xs.push(e.code));
            }
            else {
                throw (new KeyNotFound(key));
            }
        }
    }
    finally {
        enumerator.Dispose();
    }
    return init;
}

export function resolveLR(f_grammar) {
    const lr_names = new Set([]);
    const current = (rule_name, visited, _arg1) => {
        let pattern_matching_result, all, rule_name$0027_1, tl_1;
        if (!isEmpty(_arg1)) {
            if (head(_arg1).tag === 4) {
                pattern_matching_result = 0;
                all = _arg1;
            }
            else if (head(_arg1).tag === 5) {
                if (rule_name === head(_arg1).fields[0]) {
                    pattern_matching_result = 1;
                    rule_name$0027_1 = head(_arg1).fields[0];
                    tl_1 = tail(_arg1);
                }
                else {
                    pattern_matching_result = 2;
                }
            }
            else {
                pattern_matching_result = 2;
            }
        }
        else {
            pattern_matching_result = 2;
        }
        switch (pattern_matching_result) {
            case 0: {
                return [{
                    code: all,
                    kind: new LRKind(1),
                }];
            }
            case 1: {
                void addToSet(rule_name, lr_names);
                return [{
                    code: tl_1,
                    kind: new LRKind(0),
                }];
            }
            case 2: {
                let pattern_matching_result_1, all_2, rule_name$0027_3;
                if (!isEmpty(_arg1)) {
                    if (head(_arg1).tag === 5) {
                        if (contains(head(_arg1).fields[0], visited)) {
                            pattern_matching_result_1 = 0;
                            all_2 = _arg1;
                            rule_name$0027_3 = head(_arg1).fields[0];
                        }
                        else {
                            pattern_matching_result_1 = 1;
                        }
                    }
                    else {
                        pattern_matching_result_1 = 1;
                    }
                }
                else {
                    pattern_matching_result_1 = 1;
                }
                switch (pattern_matching_result_1) {
                    case 0: {
                        return [{
                            code: all_2,
                            kind: new LRKind(1),
                        }];
                    }
                    case 1: {
                        if (isEmpty(_arg1)) {
                            return [{
                                code: empty(),
                                kind: new LRKind(2),
                            }];
                        }
                        else if (head(_arg1).tag === 5) {
                            if (lr_names.has(head(_arg1).fields[0])) {
                                return [{
                                    code: _arg1,
                                    kind: new LRKind(1),
                                }];
                            }
                            else {
                                const matchValue = tryFind(head(_arg1).fields[0], f_grammar);
                                if (matchValue != null) {
                                    const prods = matchValue;
                                    const push = new FIR(6, head(_arg1).fields[0], new ScopeAction(0));
                                    const pop = new FIR(6, head(_arg1).fields[0], new ScopeAction(1));
                                    return delay(() => collect((prod) => collect((classification) => {
                                        if (classification.kind.tag === 2) {
                                            return collect((each) => singleton({
                                                code: append(singleton_1(push), append(classification.code, append(singleton_1(pop), each.code))),
                                                kind: each.kind,
                                            }), current(rule_name, visited, tail(_arg1)));
                                        }
                                        else {
                                            const c_1 = classification;
                                            return singleton({
                                                code: append(singleton_1(push), append(c_1.code, append(singleton_1(pop), tail(_arg1)))),
                                                kind: c_1.kind,
                                            });
                                        }
                                    }, current(rule_name, add(head(_arg1).fields[0], visited), prod)), prods));
                                }
                                else {
                                    const exn = new UnknownNonTerminalName(head(_arg1).fields[0]);
                                    throw exn;
                                }
                            }
                        }
                        else {
                            return delay(() => map((each_1) => ({
                                code: cons(head(_arg1), each_1.code),
                                kind: each_1.kind,
                            }), current(rule_name, visited, tail(_arg1))));
                        }
                    }
                }
            }
        }
    };
    return ofSeq(delay(() => collect((matchValue_2) => {
        const activePatternResult243 = $007CKV$007C(matchValue_2);
        const nonterminal_name = activePatternResult243[0];
        const m = groupByUnder(delay(() => collect((prod_1) => current(nonterminal_name, empty_1({
            Compare: (x, y) => comparePrimitives(x, y),
        }), prod_1), activePatternResult243[1])));
        if ((!isEmpty_1(FSharpMap__get_Item(m, new LRKind(0)))) ? (!isEmpty_1(FSharpMap__get_Item(m, new LRKind(2)))) : false) {
            const exn_1 = new NullableLeftRecursion(nonterminal_name);
            throw exn_1;
            return empty_2();
        }
        else {
            return singleton([nonterminal_name, new FGrammarExRHS(ofSeq_1(FSharpMap__get_Item(m, new LRKind(0))), ofSeq_1(FSharpMap__get_Item(m, new LRKind(1))), ofSeq_1(FSharpMap__get_Item(m, new LRKind(2))))]);
        }
    }, f_grammar)));
}

