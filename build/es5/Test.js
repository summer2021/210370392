import { ScopeAction, pgrammar_to_fgrammar, FIR, flatten_parsing_expression, cartesian_product, ProdRHS, CLang, PE } from "./Compiler/IR.js";
import { empty, ofSeq as ofSeq_1, ofArray, singleton } from "./.fable/fable-library.3.2.9/List.js";
import { ofList as ofList_1, ofSeq } from "./.fable/fable-library.3.2.9/Map.js";
import { printf, toConsole, replicate } from "./.fable/fable-library.3.2.9/String.js";
import { map, collect, delay, toList } from "./.fable/fable-library.3.2.9/Seq.js";
import { ofSeq as ofSeq_2, ofList } from "./.fable/fable-library.3.2.9/Set.js";
import { comparePrimitives, compare } from "./.fable/fable-library.3.2.9/Util.js";
import { FGrammarExRHS, resolveLR } from "./Compiler/LR.js";
import { firstSets } from "./Compiler/CG.js";

export const v = new PE(1, ofArray([new PE(0, singleton(new PE(4, "c"))), new PE(0, ofArray([new PE(1, ofArray([new PE(4, "a"), new PE(4, "c")])), new PE(5, "a"), new PE(0, ofArray([new PE(4, "a"), new PE(4, "b")]))]))]));

export const lr = ofSeq([["a", ofArray([new ProdRHS(new PE(0, ofArray([new PE(5, "a"), new PE(4, "b")])), new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)]))), new ProdRHS(new PE(0, singleton(new PE(4, "b"))), void 0)])], ["c", singleton(new ProdRHS(new PE(0, ofArray([new PE(5, "d"), new PE(4, "b")])), new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)]))))], ["d", ofArray([new ProdRHS(new PE(0, ofArray([new PE(5, "c"), new PE(4, "b")])), new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)]))), new ProdRHS(new PE(0, singleton(new PE(4, "e"))), void 0)])]]);

export const WIDTH = 70;

export function testcase(text, f) {
    const n = text.length | 0;
    if (n < WIDTH) {
        const side = replicate(~(~((WIDTH - n) / 2)), "=");
        toConsole(printf("%s%s%s"))(side)(text)(side);
    }
    else {
        toConsole(printf("%s"))(text);
    }
    f();
    toConsole(printf("Test Pass"));
}

(function (_arg1) {
    try {
        testcase("Test Cartesian Product", () => {
            const y = cartesian_product(ofArray([ofArray([1, 2]), ofArray([2, 3, 5]), ofArray([5, 6])]));
            FsUnit_TopLevelOperators_should((x) => FsUnit_TopLevelOperators_equal(x), ofSeq_1(toList(delay(() => collect((c) => collect((b) => map((a) => ofArray([a, b, c]), [1, 2]), [2, 3, 5]), [5, 6])))), y);
        });
        testcase("Test PGrammar -\u003e FGrammar", () => {
            const y_3 = ofList(ofSeq_1(flatten_parsing_expression(v)), {
                Compare: (x_2, y_1) => compare(x_2, y_1),
            });
            FsUnit_TopLevelOperators_should((x_3) => FsUnit_TopLevelOperators_equal(x_3), ofSeq_2([ofArray([new FIR(4, "c"), new FIR(1, 1)]), ofArray([new FIR(4, "a"), new FIR(5, "a"), new FIR(4, "a"), new FIR(4, "b"), new FIR(1, 2), new FIR(1, 3)]), ofArray([new FIR(4, "c"), new FIR(5, "a"), new FIR(4, "a"), new FIR(4, "b"), new FIR(1, 2), new FIR(1, 3)])], {
                Compare: (x_4, y_2) => compare(x_4, y_2),
            }), y_3);
        });
        testcase("Test PGrammar -\u003e FGrammar -\u003e FGrammarEx", () => {
            const y_4 = resolveLR(pgrammar_to_fgrammar(lr));
            FsUnit_TopLevelOperators_should((x_6) => FsUnit_TopLevelOperators_equal(x_6), ofList_1(ofArray([["a", new FGrammarExRHS(singleton(ofArray([new FIR(4, "b"), new FIR(1, 2), new FIR(3, new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)])))])), singleton(ofArray([new FIR(4, "b"), new FIR(1, 1)])), empty())], ["c", new FGrammarExRHS(singleton(ofArray([new FIR(6, "d", new ScopeAction(0)), new FIR(4, "b"), new FIR(1, 2), new FIR(3, new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)]))), new FIR(6, "d", new ScopeAction(1)), new FIR(4, "b"), new FIR(1, 2), new FIR(3, new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)])))])), singleton(ofArray([new FIR(6, "d", new ScopeAction(0)), new FIR(4, "e"), new FIR(1, 1), new FIR(6, "d", new ScopeAction(1)), new FIR(4, "b"), new FIR(1, 2), new FIR(3, new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)])))])), empty())], ["d", new FGrammarExRHS(empty(), ofArray([ofArray([new FIR(5, "c"), new FIR(4, "b"), new FIR(1, 2), new FIR(3, new CLang(8, new CLang(1, "cons", false), ofArray([new CLang(0, 1), new CLang(0, 2)])))]), ofArray([new FIR(4, "e"), new FIR(1, 1)])]), empty())]])), y_4);
        });
        testcase("JSON", () => {
            FsUnit_TopLevelOperators_should((x_8) => FsUnit_TopLevelOperators_equal(x_8), "[\n  {\n    \"CVar\": [\n      \"a\",\n      false\n    ]\n  },\n  {\n    \"CInt\": 1\n  }\n]", FSharp_Json_Json_serialize(ofArray([new CLang(1, "a", false), new CLang(3, 1)])));
        });
        testcase("firstsets", () => {
            const y_9 = firstSets(resolveLR(pgrammar_to_fgrammar(lr)));
            FsUnit_TopLevelOperators_should((x_10) => FsUnit_TopLevelOperators_equal(x_10), ofList_1(ofArray([["a", ofSeq_2(["b"], {
                Compare: (x_11, y_6) => comparePrimitives(x_11, y_6),
            })], ["c", ofSeq_2(["e"], {
                Compare: (x_12, y_7) => comparePrimitives(x_12, y_7),
            })], ["d", ofSeq_2(["e"], {
                Compare: (x_13, y_8) => comparePrimitives(x_13, y_8),
            })]])), y_9);
        });
    }
    catch (e) {
        toConsole(printf("exception: %A  !!!"))(e);
    }
    return 0;
})(typeof process === 'object' ? process.argv.slice(2) : []);

