module TypedPG.Compiler.CG
open TypedPG.Compiler.IR
open TypedPG.Compiler.LR
type TokenValues = Map<string, int>

let bof_token_id = -1
let eof_token_id = -2

let firstSets (g: FGrammarEx) =
    let m = System.Collections.Generic.Dictionary<string, _>()
    for KV(n, _) in g do
        m.[n] <- System.Collections.Generic.HashSet<string>()
    
    let rec applyEach n =
        if m.ContainsKey n then
            m.[n]
        else
        let first_set = System.Collections.Generic.HashSet<string>()
        m.[n] <- first_set
        for prod in g.[n].NotLR do
            applyEachWithProds n prod |> ignore
        first_set
    
    and applyEachWithProds n notLR_prod =
        let first_set = m.[n]
        match notLR_prod with
        | [] -> ()
        | FTerm term::_ ->
            first_set.Add term |> ignore
        | FNonTerm n'::_ ->
            Seq.iter (first_set.Add>>ignore) (applyEach n')
        | _::tl -> applyEachWithProds n tl

    for KV(n, prods) in g do
        for prod in prods.NotLR do
            applyEachWithProds n prod |> ignore

    Map.ofSeq <|
    seq {
        for KV(k, v) in m do
            k, Set.ofSeq v
    }


let codegen : TokenValues -> FGrammarEx -> CLang = fun token_values g ->
    failwith ""
