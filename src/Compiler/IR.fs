module TypedPG.Compiler.IR
//Intermediate representation


let (|KV|) (kv: System.Collections.Generic.KeyValuePair<_, _>) = KV(kv.Key, kv.Value)

// Common Language
(* 设计用来书写Semantic Action的编程语言，
   目的是让它能够编译到Julia、C、C++、Java等不同后端，
   能较为方便的进行类型推导和语义分析 *)
type CLang =
    (* a = b c { ($1, $2) }
                  ^^ *)
    | CSlot of int // 代码生成阶段会消除

    // IDENTIFIER
    | CVar of name: string * is_builtin: bool
    // some literals
    | CNone
    | CInt of int
    | CFloat of float
    | CStr of string
    | CBool of bool

    // some compound expressions
    | CTuple of List<CLang>
    | CCall of CLang * List<CLang>
    | CAttr of CLang * string
    | CBlock of List<CLang>
    | CAnd of CLang * CLang
    | COr of CLang * CLang
    | CProj of CLang * int

    // 只在代码生成阶段出现
    | CDefun of string * parameters: string list * body: CLang
    | CAssign of name: string * CLang
    | CSwitch of CLang * List<int * CLang> * CLang
    | CWhile of CLang * CLang
    | CIf of CLang * CLang * CLang


// parsing expression
type PE =
    (* a ::= (a b) c
             ^^^^^ *)
    | PAnd of List<PE>

    (* a ::= (a | b)  c
              ^^^^^^ *)
    | POr of List<PE>

    (* a : a? <A>;
           ^^ **)
    | POpt of PE

    (*    a : a b c name=d { e };
                    ^^^^^^ *)
    | PBind of name: string * PE


    | PTerm of string // terminal
    | PNonTerm of string // nontermal

// production right-hand side
type ProdRHS = { parser: PE; action: CLang option }

// parsing expression grammar type
type PGrammar = Map<string, ProdRHS list>

// flatten IR, 一种Stack-based IR
(* 用来描述parsing行为的高层语义指令集 *)
(* 从 PE -> FIR的编译示例:
   输入: <b> <c> d
   输出:
          FIR文本表示      栈元素变化      作用域变化
          term b        [<b>] <- 栈顶       {}
          term c        [<b>, <c>]         {}
          nonterm d     [<b>, <c>, d]      {}
          store name    [<b>, <c>, d]      {name : d}
*)
type FIR =
    (* tos = peek(0)
       regs[name] = tos *)
    | FStore of name: string

    (* N = count
       tos_N = pop(); ...; tos_1 = pop()
       push((tos1, ..., tosN)) *)
    | FPack of count: int

    (*  tos = pop()
        cst_node = Node(name, tos)
        push(cst_node) *)
    (* 例子:
        stack = [..., (<a>, <b>, d)]
        执行下列指令
            fmcst "a"
        得到
        stack = [..., Node("a", (<a>, <b>, d))] *)
    | FMkCST of name: string

    (*
        pop()
        result = evaluate code under regs
        push(result)
    *)
    | FExpr of code: CLang

    | FTerm of string // parse terminal

    | FNonTerm of string // parse non-terminal

    // push/pop scope
    (* 用于左递归处理：
       左递归处理需要文法inline, 我们用这种办法来保持原文法信息。*)
    | FScope of nonterminal_name: string * ScopeAction

and ScopeAction =
    | Push
    | Pop

type FCode = FIR list
type FGrammar = Map<string, FCode list>



let rec cartesian_product: list<list<'a>> -> list<list<'a>> =
    function
    | [] -> [ [] ] // [ [] ]
    | heads :: tails ->
        seq {
            for tl in cartesian_product tails do
                for hd in heads do
                    yield hd :: tl
        }
        |> Seq.toList


let push_back a xs = xs @ [ a ]



// rule ::= <B> <C> a
//      | ...
// can be flatten to
(* 给定规则 rule ::= <B> <C> a
   对应的PE是 PAnd [PTerm "B"; PTerm "C"; PNonTerm "a"]
   我们将其处理为如下FIR:
    FIR                  | Stack
    ---------------------|----------------------------
    scope("rule", push)  | []
    term "B"             | [<B>]
    term "C"             | [<B>, <C>]
    nonterm "a"          | [<B>, <C>, a]
    pack 3               | [(<B>, <C>, a)]
    mkcst "rule"         | [Node("rule", (<B>, <C>, a)]
    scope("rule", pop)   | [Node("rule", (<B>, <C>, a)]
 *)

let rec flatten_parsing_expression: PE -> FCode list =
    function
    | PAnd elements ->
        let len = List.length elements
        let pack = FPack len

        elements
        |> List.map (flatten_parsing_expression)
        // output[i] \in fpe(elements[i])
        |> cartesian_product
        // 每一个可能的规则结尾都需要插入 (FPack len)
        |> List.map (List.concat >> push_back pack)
    | POr alternatives ->
        alternatives
        |> List.map flatten_parsing_expression
        // 合并可能的结果
        |> List.concat

    | PTerm name -> [ [ FTerm name ] ]
    | PNonTerm name -> [ [ FNonTerm name ] ]
    | PBind (name, pe) ->
        let store = FStore name

        pe
        |> flatten_parsing_expression
        |> List.map (push_back store)
    | POpt pe ->
        pe
        |> flatten_parsing_expression
        // 空规则输出长度为0的tuple
        |> List.append [ [ FPack 0 ] ] // FPack 0

module Seq =
    let groupBy func seq =
        Seq.fold
            (fun state e ->
                let key = func e

                match Map.tryFind key state with
                | None ->
                    let xs = System.Collections.Generic.List()
                    xs.Add e |> ignore
                    Map.add key xs state
                | Some xs ->
                    xs.Add e |> ignore
                    state)
            Map.empty
            seq

let pgrammar_to_fgrammar (pgrammar: PGrammar) : FGrammar =
    Map.ofSeq
    <| seq {
        for KV (k, prods) in pgrammar do
            k,
            List.ofSeq
            <| seq {
                for { parser = parser; action = action } in prods do
                    yield!
                        flatten_parsing_expression parser
                        |> match action with
                           | None -> id
                           | Some expr ->
                               let action = FExpr expr
                               List.map (push_back action)
               }

       }
