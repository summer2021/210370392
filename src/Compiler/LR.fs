module TypedPG.Compiler.LR

open TypedPG.Compiler.IR
// FGrammar with left recursions classified

type FGrammarExRHS =
    { LR: List<FCode>
      NotLR: List<FCode>
      IsEmpty: List<FCode> }


(* 每一个nonterminal symbol
   可以衍生很多种FCode(FIR list),
   每一个FCode属于以下类别之一)
*)
type LRKind =
    | IsLR // 左递归情况
    | NotLR // 非左递归（非空）
    | IsEmpty // 非左递归（空规则）

// 在FGrammar的基础上，进一步把每一个FCode划分为
// IsLR, NotLR, IsEmpty等三种
type FGrammarEx = Map<string, FGrammarExRHS>

exception KeyNotFound of obj
exception UnknownNonTerminalName of string
exception NullableLeftRecursion of nonterminal_name: string


let groupByUnder (seq: {| kind: LRKind; code: FCode |} seq) =
    let init =
        Map.ofList [ for each in [ IsLR; NotLR; IsEmpty ] -> each, System.Collections.Generic.List() ]

    for e in seq do
        let key = e.kind

        match Map.tryFind key init with
        | None -> raise (KeyNotFound key)
        | Some xs -> xs.Add e.code |> ignore

    init

// 解析左递归，将FGrammar转为FGrammarEx
let resolveLR: FGrammar -> FGrammarEx =
    fun f_grammar ->
        let lr_names =
            System.Collections.Generic.HashSet<string>()

        // 在确定非终止符符号(rule_name)的情况下，
        // 尝试展开FCode的第一项
        let rec current rule_name visited =
            function
            // 第一项是终止符，该FCode NotLR
            | FTerm _ :: _ as all -> seq [ {| kind = NotLR; code = all |} ]
            // 第一项是非终止符且和rule_name相同，该FCode IsLR
            | FNonTerm rule_name' :: tl when rule_name = rule_name' ->
                lr_names.Add rule_name |> ignore
                seq [ {| kind = IsLR; code = tl |} ]
            // 第一项是非终止符且和rule_name不同，且已经被访问过，该终止符包含左递归FCode，
            // 且当前FCode不是左递归的
            | FNonTerm rule_name' :: _ as all when Set.contains rule_name' visited ->
                seq [ {| kind = NotLR; code = all |} ]
            // 第一项是非终止符且和rule_name不同，且未被访问过。
            // 进行inline操作
            | FNonTerm rule_name' :: tl as all ->
                if lr_names.Contains rule_name' then
                    seq [ {| kind = NotLR; code = all |} ]
                else
                    match Map.tryFind rule_name' f_grammar with
                    | None -> raise <| UnknownNonTerminalName rule_name'
                    | Some (prods: List<FCode>) ->
                        let push = FScope(rule_name', Push)
                        let pop = FScope(rule_name', Pop)

                        seq {
                            for prod in prods do
                                for classification in current rule_name (Set.add rule_name' visited) prod do
                                    match classification.kind with
                                    | IsEmpty ->
                                        for each in current rule_name visited tl do
                                            let c = classification

                                            yield
                                                {| each with
                                                       code = [ push ] @ c.code @ [ pop ] @ each.code |}
                                    | _ ->
                                        let c = classification

                                        yield
                                            {| c with
                                                   code = [ push ] @ c.code @ [ pop ] @ tl |}
                        }
            // 还没找到第一项，继续处理
            | hd :: tl ->
                seq {
                    for each in current rule_name visited tl do
                        yield {| each with code = hd :: each.code |}
                }
            // 发现空规则
            | [] -> seq [ {| kind = IsEmpty; code = [] |} ]

        Map.ofSeq
        <| seq {
            for KV (nonterminal_name, prods) in f_grammar do
                let m =
                    seq {
                        for prod in prods do
                            yield! current nonterminal_name Set.empty prod
                    }
                    |> groupByUnder
                // printfn "%A" <| Map.map (fun _ v -> List.ofSeq v) m
                if
                    not (Seq.isEmpty m.[IsLR])
                    && not (Seq.isEmpty m.[IsEmpty])
                then
                    raise <| NullableLeftRecursion nonterminal_name
                else

                    yield
                        (nonterminal_name,
                         { LR = List.ofSeq m.[IsLR]
                           NotLR = List.ofSeq m.[NotLR]
                           IsEmpty = List.ofSeq m.[IsEmpty] })
           }
