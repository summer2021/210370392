module Test

open TypedPG.Compiler.IR
open TypedPG.Compiler.LR
open TypedPG.Compiler.CG
open FSharp.Json
open FsUnit

let v =
    POr [ PAnd [ PTerm "c" ]
          PAnd [ POr [ PTerm "a"; PTerm "c" ]
                 PNonTerm "a"
                 PAnd [ PTerm "a"; PTerm "b" ] ] ]


let lr =
    Map.ofSeq [ "a",
                [ { parser = PAnd [ PNonTerm "a"; PTerm "b" ]
                    action =
                        CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])
                        |> Some }
                  { parser = PAnd [ PTerm "b" ]
                    action = None } ]
                "c",
                [ { parser = PAnd [ PNonTerm "d"; PTerm "b" ]
                    action =
                        CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])
                        |> Some } ]

                "d",
                [ { parser = PAnd [ PNonTerm "c"; PTerm "b" ]
                    action =
                        CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])
                        |> Some }
                  { parser = PAnd [ PTerm "e" ]
                    action = None } ] ]


let WIDTH = 70

let testcase text f =
    let n = String.length text

    if n < WIDTH then
        let k = (WIDTH - n) / 2
        let side = String.replicate k "="
        printfn "%s%s%s" side text side
    else
        printf "%s" text

    f ()
    printfn "Test Pass"

[<EntryPoint>]
let main _ =
    try
        testcase "Test Cartesian Product"
        <| fun () ->

            [ [ 1; 2 ]; [ 2; 3; 5 ]; [ 5; 6 ] ]
            |> cartesian_product
            |> should
                equal
                (List.ofSeq [ for c in [ 5; 6 ] do
                                  for b in [ 2; 3; 5 ] do
                                      for a in [ 1; 2 ] do
                                          yield [ a; b; c ] ])

        testcase "Test PGrammar -> FGrammar"
        <| fun () ->

            v
            |> flatten_parsing_expression
            |> List.ofSeq
            |> Set.ofList
            |> should
                equal
                (set [ [ FTerm "c"; FPack 1 ]
                       [ FTerm "a"
                         FNonTerm "a"
                         FTerm "a"
                         FTerm "b"
                         FPack 2
                         FPack 3 ]
                       [ FTerm "c"
                         FNonTerm "a"
                         FTerm "a"
                         FTerm "b"
                         FPack 2
                         FPack 3 ] ]

                )


        testcase "Test PGrammar -> FGrammar -> FGrammarEx"
        <| fun () ->

            lr
            |> pgrammar_to_fgrammar
            |> resolveLR
            |> should
                equal
                (Map.ofList [ ("a",
                               { LR =
                                     [ [ FTerm "b"
                                         FPack 2
                                         FExpr(CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])) ] ]
                                 NotLR = [ [ FTerm "b"; FPack 1 ] ]
                                 IsEmpty = [] })
                              ("c",
                               { LR =
                                     [ [ FScope("d", Push)
                                         FTerm "b"
                                         FPack 2
                                         FExpr(CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ]))
                                         FScope("d", Pop)
                                         FTerm "b"
                                         FPack 2
                                         FExpr(CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])) ] ]
                                 NotLR =
                                     [ [ FScope("d", Push)
                                         FTerm "e"
                                         FPack 1
                                         FScope("d", Pop)
                                         FTerm "b"
                                         FPack 2
                                         FExpr(CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])) ] ]
                                 IsEmpty = [] })
                              ("d",
                               { LR = []
                                 NotLR =
                                     [ [ FNonTerm "c"
                                         FTerm "b"
                                         FPack 2
                                         FExpr(CCall(CVar("cons", false), [ CSlot 1; CSlot 2 ])) ]
                                       [ FTerm "e"; FPack 1 ] ]
                                 IsEmpty = [] }) ]

                )

        testcase "JSON"
        <| fun () ->
            [ CVar("a", false); CInt 1 ]
            |> Json.serialize
            |> should
                equal
                ("""[
  {
    "CVar": [
      "a",
      false
    ]
  },
  {
    "CInt": 1
  }
]"""

                )

        testcase "firstsets"
        <| fun () ->
            lr
            |> pgrammar_to_fgrammar
            |> resolveLR
            |> firstSets
            |> should
                equal
                (Map.ofList [ ("a", set [ "b" ])
                              ("c", set [ "e" ])
                              ("d", set [ "e" ]) ])
    with
    | e -> printfn "exception: %A  !!!" e

    0
